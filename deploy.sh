set -e

fuser -k 8000/tcp || echo "App not running"
nohup python3 manage.py runserver 0.0.0.0:8000